package ro.ubbcluj.map;

import ro.ubbcluj.map.model.*;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.repository.db.FriendRequestDbRepository;
import ro.ubbcluj.map.repository.db.FriendshipDbRepository;
import ro.ubbcluj.map.repository.db.MessageDbRepository;
import ro.ubbcluj.map.repository.db.UtilizatorDbRepository;
import ro.ubbcluj.map.repository.Repository;
import ro.ubbcluj.map.service.FriendRequestService;
import ro.ubbcluj.map.service.FriendshipService;
import ro.ubbcluj.map.service.MessageService;
import ro.ubbcluj.map.service.UserService;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        Repository<Long, User> repoDb = new UtilizatorDbRepository("jdbc:postgresql://localhost:5432/userApp", "postgres", "qwaszx12");
        Repository<Tuple<Long, Long>, Friendship> repoDbf = new FriendshipDbRepository("jdbc:postgresql://localhost:5432/userApp", "postgres", "qwaszx12");
        Repository<Long, MessageDTO> messageDb = new MessageDbRepository("jdbc:postgresql://localhost:5432/userApp", "postgres", "qwaszx12");
        Repository<Tuple<Long, Long>, FriendRequest> frRequestDb = new FriendRequestDbRepository("jdbc:postgresql://localhost:5432/userApp", "postgres", "qwaszx12");

        UserService userService = new UserService(repoDb, repoDbf);
        FriendshipService friendshipService = new FriendshipService(repoDb, repoDbf);
        MessageService messageService = new MessageService(messageDb, repoDb, repoDbf);
        FriendRequestService friendRequestService = new FriendRequestService(frRequestDb, repoDb, repoDbf);

        boolean ok = true;

        while (ok) {
            System.out.println("1.Add an user\n2.Remove an user\n3.Add a friendship\n4.Remove a friendship\n" +
                    "5.Number of conencted components\n6.The most sociable community\n7.The friendships of a user\n8.The friendships of a user by month\n9.Send a message\n10.Reply a message\n" +
                    "11.Get a conversation\n12.Send a friend request\n13.Accept a friend request\n14.Reject a friend request\n15.Exit");
            System.out.println("Choose an option");
            Scanner in = new Scanner(System.in);
            int opt = Integer.parseInt(in.nextLine());
            switch (opt) {
                case 1 -> {

                    System.out.println("First name:");
                    String fname = in.nextLine();
                    System.out.println("Last name:");
                    String lname = in.nextLine();

                    User user = new User(fname, lname);

                    try {
                        userService.add(user);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 2 -> {

                    System.out.println("Id:");
                    long id = Long.parseLong(in.nextLine());
                    try {
                        userService.remove(id);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 3 -> {

                    System.out.println("First id: ");
                    Long fid = Long.parseLong(in.nextLine());
                    System.out.println("Second id: ");
                    Long sid = Long.parseLong(in.nextLine());

                    if (fid > sid) {
                        Long aux = fid;
                        fid = sid;
                        sid = aux;
                    }

                    Tuple<Long, Long> ship = new Tuple<>(fid, sid);
                    Friendship friendship = new Friendship(ship);
                    friendship.setId(ship);
                    try {
                        friendshipService.add(friendship);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 4 -> {

                    System.out.println("First friend id");
                    long id = Long.parseLong(in.nextLine());
                    System.out.println("Second friend id");
                    long id1 = Long.parseLong(in.nextLine());

                    if (id > id1) {
                        Long aux = id;
                        id = id1;
                        id1 = aux;
                    }

                    try {
                        friendshipService.removeFriendship(id, id1);
                    } catch (ValidationException | IllegalArgumentException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 5 -> System.out.println("The number of connected components is: " + userService.nrConnectedComponents());
                case 6 -> {

                    System.out.println("The most sociable community is:");
                    userService.sociableCommunity().forEach(System.out::println);
                }
                case 7 -> {

                    System.out.println("Id");
                    long id = Long.parseLong(in.nextLine());
                    try {
                        friendshipService.getFriendships(id).forEach(x -> {
                            System.out.println(x.getUser().getLastName() + " " + x.getUser().getFirstName() + " " + x.getDate());
                        });
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 8 -> {

                    System.out.println("Id: ");
                    long id = Long.parseLong(in.nextLine());
                    System.out.println("Month: ");
                    String month = in.nextLine();
                    try {
                        List<FriendshipDTO> resultList = friendshipService.getFriendshipsByMonth(id, month);
                        if (resultList.size() > 0) {
                            resultList.forEach(x ->
                                    System.out.println(x.getUser().getFirstName() + " " + x.getUser().getLastName() + " " + x.getDate()));
                        } else {
                            System.out.print("No friends added in " + month + "\n");
                        }
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 9 -> {
                    System.out.println("From (id):");
                    long from = Long.parseLong(in.nextLine());
                    System.out.println("To (id):");
                    List<Long> tos = new ArrayList<>();
                    Long to = Long.parseLong(in.nextLine());
                    while (to != -1) {
                        tos.add(to);
                        to = Long.parseLong(in.nextLine());
                    }
                    System.out.println("Enter the message:");
                    String message = in.nextLine();
                    try {
                        messageService.sendMessage(from, tos, message);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 10 -> {
                    System.out.println("To (idMessage):");
                    long from = Long.parseLong(in.nextLine());
                    System.out.println("From (idUser):");
                    long to = Long.parseLong(in.nextLine());
                    System.out.println("Enter the message:");
                    String message = in.nextLine();
                    try {
                        messageService.replyToOne(from, to, message);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 11 -> {
                    System.out.println("First friend id");
                    long id = Long.parseLong(in.nextLine());
                    System.out.println("Second friend id");
                    long id1 = Long.parseLong(in.nextLine());

                    if (id > id1) {
                        Long aux = id;
                        id = id1;
                        id1 = aux;
                    }
                    try {
                        messageService.listConversation(messageService.getConversation(id, id1));
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 12 -> {
                    System.out.println("Who send friend request: (user ID)");
                    long from = Long.parseLong(in.nextLine());
                    System.out.println("Send friend request to: (userID)");
                    long to = Long.parseLong(in.nextLine());

                    if (from > to) {
                        Long aux = from;
                        from = to;
                        to = aux;
                    }
                    try {
                        friendRequestService.sendFriendRequest(from, to);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 13 ->{
                    System.out.println("Who send friend request: (user ID)");
                    long from = Long.parseLong(in.nextLine());
                    System.out.println("Send friend request to: (userID)");
                    long to = Long.parseLong(in.nextLine());

                    if (from > to) {
                        Long aux = from;
                        from = to;
                        to = aux;
                    }
                    try {
                        friendRequestService.acceptFriendRequest(from, to);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 14 -> {
                    System.out.println("Who send friend request: (user ID)");
                    long from = Long.parseLong(in.nextLine());
                    System.out.println("Send friend request to: (userID)");
                    long to = Long.parseLong(in.nextLine());

                    if (from > to) {
                        Long aux = from;
                        from = to;
                        to = aux;
                    }
                    try {
                        friendRequestService.rejectFriendRequest(from, to);
                    } catch (ValidationException e) {
                        System.out.println(e.getMessage());
                    }
                }
                case 15 -> {

                    System.out.println("Bye!");
                    ok = false;
                }
                default -> System.out.println("Invalid option");
            }
        }
    }
}