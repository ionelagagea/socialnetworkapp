package ro.ubbcluj.map.service;

import ro.ubbcluj.map.model.*;
import ro.ubbcluj.map.model.validators.ValidationException;
import ro.ubbcluj.map.repository.Repository;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.function.Predicate;

public class FriendshipService {
    Repository<Long, User> repoUser;
    Repository<Tuple<Long, Long>, Friendship> repoFriendship;

    /**
     * @param repoUser
     * @param repoFriendship
     */
    public FriendshipService(Repository repoUser, Repository<Tuple<Long, Long>, Friendship> repoFriendship) {
        this.repoUser = repoUser;
        this.repoFriendship = repoFriendship;
    }

    /**
     * @param entity
     * @return entity if saved
     * throw Validate Exception if the ID already exists or it is invalid
     */
    public Friendship add(Friendship entity) {
        Long id1 = entity.getE1();
        Long id2 = entity.getE2();

        if (repoUser.findOne(id1) == null || repoUser.findOne(id2) == null)
            throw new ValidationException("Id invalid");
        if (repoFriendship.findOne(entity.getId()) != null)
            throw new ValidationException("Already exists");

        repoUser.findOne(id1).addFriend(repoUser.findOne(id2));
        repoUser.findOne(id2).addFriend(repoUser.findOne(id1));
        LocalDateTime dateTime = LocalDateTime.now();

        entity.setDate(dateTime);
        return repoFriendship.save(entity);
    }


    /**
     * @param id1 must be not null
     * @param id2 must be not null
     */
    public void removeFriendship(Long id1, Long id2) {
        if (repoUser.findOne(id1) == null || repoUser.findOne(id2) == null)
            throw new ValidationException("Nu exista utilizatorii introdusi!");
        Tuple<Long, Long> ship = new Tuple<>(id1, id2);
        repoFriendship.remove(repoFriendship.findOne(ship));
        //repoFriendship.setFriendships();
    }

    /**
     * return all the friendships of a user
     *
     * @param id
     * @return
     */
    public List<FriendshipDTO> getFriendships(Long id) {
        if (repoUser.findOne(id) == null)
            throw new ValidationException("Invalid id");
        List<FriendshipDTO> friendslist = new ArrayList<>();
        Iterable<Friendship> friendshipIterable = repoFriendship.findAll();

        Predicate<Friendship> firstfriend = x -> x.getE1().equals(id);
        Predicate<Friendship> secondfriend = x -> x.getE2().equals(id);
        Predicate<Friendship> friendshipPredicate = firstfriend.or(secondfriend);
        List<Friendship> list = new ArrayList<>();
        friendshipIterable.forEach(list::add);
        list.stream().filter(friendshipPredicate).map(x -> {
            if (x.getE1().equals(id))
                return new FriendshipDTO(repoUser.findOne(x.getE2()), x.getDate());
            else
                return new FriendshipDTO(repoUser.findOne(x.getE1()), x.getDate());
        }).forEach(friendslist::add);

        return friendslist;
    }

    /** Get all friends of a user by id and month
     * @param id    Long - the id of the user
     * @param month String - the name of the month
     * @return a user's friends list by month
     */
    public List<FriendshipDTO> getFriendshipsByMonth(Long id, String month) {
        if (repoUser.findOne(id) == null)
            throw new ValidationException("Invalid user id!");

        final List<String> allMonths = Arrays.asList("january", "february", "march", "april", "may", "june", "july", "august", "september", "october", "november", "december");

        if (!allMonths.contains(month.toLowerCase()))
            throw new ValidationException("Incorrect month!");

        List<FriendshipDTO> friendshipDTOList = getFriendships(id);
        List<FriendshipDTO> result = new ArrayList<>();

        Predicate<FriendshipDTO> friendshipDTOPredicate = x -> String.valueOf(x.getDate().getMonth().toString()).equals(month.toUpperCase());
        friendshipDTOList.stream().filter(friendshipDTOPredicate).forEach(result::add);

        return result;
    }
}